from flask.cli import FlaskGroup

from project import create_app, db
from project.api.productsites import Productsite, Factoruser


app = create_app()
cli = FlaskGroup(create_app=create_app)


@cli.command('recreate_db')
def recreate_db():
    db.drop_all()
    db.create_all()
    db.session.commit()


@cli.command('seed_db')
def seed_db():
    """Seeds the database."""
    db.session.add(Factoruser(factoruser_name='nata', factoruser_password='natapass'))
    db.session.add(Productsite(factoruser_id=1, productsite_name='PTM'))
    db.session.commit()


if __name__ == '__main__':
    cli()
