#!/usr/bin/env bash

set -o errexit
set -o pipefail

# Check if env vars are set - if not assign defaults
if [[ -z ${FLASK_APP} ]]; then
  FLASK_APP=productsites.py
else
  FLASK_APP=$FLASK_APP
fi

if [[ -z ${FLASK_DEBUG} ]]; then
  FLASK_DEBUG=1
else
  FLASK_DEBUG=$FLASK_DEBUG
fi

echo "Waiting for postgres..."

while ! nc -z ${SQL_HOST} 5432; do
  sleep 0.1
done

echo "PostgreSQL started"

gunicorn -b 0.0.0.0:5000 manage:app