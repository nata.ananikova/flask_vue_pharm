from flask import Flask, Blueprint, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from sqlalchemy import DateTime, func, desc
from project import db
from flask_cors import CORS
import os
from dataclasses import dataclass


factorusers_blueprint = Blueprint('factorusers', __name__)
productsites_blueprint = Blueprint('productsites', __name__)
productitems_blueprint = Blueprint('productitems', __name__)

app = Flask(__name__)

ma = Marshmallow(app)


# enable CORS
CORS(app, resources={r'/*': {'origins': '*'}})


@dataclass
class Factoruser(db.Model):
    __tablename__ = "factoruser"

    factoruser_id: int
    factoruser_name: str
    factoruser_password: str

    factoruser_id = db.Column(db.Integer, primary_key=True)
    factoruser_name = db.Column(db.String(50))
    factoruser_password = db.Column(db.String(50))

    factoruser_productsite_data = db.relationship("Productsite", backref="Factoruser")


class FactoruserSchema(ma.Schema):
    class Meta:
        fields = ("factoruser_id", "factoruser_name", "factoruser_password")
        model = Factoruser


factoruser_schema = FactoruserSchema()
factorusers_schema = FactoruserSchema(many=True)

@dataclass
class Productsite(db.Model):
    __tablename__ = "productsite"

    productsite_id: int
    factoruser_id: int
    productsite_name: str

    productsite_id = db.Column(db.Integer, primary_key=True)
    factoruser_id = db.Column(db.ForeignKey('factoruser.factoruser_id'))
    productsite_name = db.Column(db.String)

    productsite_productitem_data = db.relationship("Productitem", backref="Productsite")


class ProductsiteSchema(ma.Schema):
    class Meta:
        fields = ("productsite_id", "factoruser_id", "productsite_name")
        model = Productsite


productsite_schema = ProductsiteSchema()
productsites_schema = ProductsiteSchema(many=True)


@dataclass
class Productitem(db.Model):
    __tablename__ = "productitem"

    productitem_id: int
    productsite_id: int
    productitem_name: str
    productitem_batch: str
    productitem_checked: str
    productitem_shift: str
    productitem_date: str
    dayString: str
    productitem_type_sanitary: str
    productitem_time_created: DateTime

    productitem_id = db.Column(db.Integer, primary_key=True)
    productsite_id = db.Column(db.ForeignKey('productsite.productsite_id'))
    productitem_name = db.Column(db.String)
    productitem_batch = db.Column(db.String)
    productitem_checked = db.Column(db.String)
    productitem_shift = db.Column(db.String)
    productitem_date = db.Column(db.String)
    dayString = db.Column(db.String)
    productitem_type_sanitary = db.Column(db.String)
    productitem_time_created = db.Column(DateTime(timezone=True), server_default=func.now())


class ProductitemSchema(ma.Schema):
    class Meta:
        fields = ("productitem_id", "productsite_id", "productitem_name", "productitem_batch", "productitem_checked",
                  "productitem_shift", "productitem_date", "dayString", "productitem_type_sanitary", "productitem_time_created")
        model = Productitem


productitem_schema = ProductitemSchema()
productitems_schema = ProductitemSchema(many=True)


@productsites_blueprint.route('/pharm/productsites/get/all', methods=['GET'])
def all_productsites():
        lst = []
        for i in Productsite.query.all():
            lst.append(i.productsite_id)
        return jsonify({'productsites': Productsite.query.all(), 'productsites_id': lst})


@productsites_blueprint.route('/pharm/productsites/add', methods=['POST'])
def add_productsite():
    db.session.add(Productsite(factoruser_id=1, productsite_name=request.json['productsite_name']))
    db.session.commit()
    return jsonify({'message': 'Productsite added!'})


@productsites_blueprint.route('/pharm/productsites/update', methods=['PUT'])
def update_productsite():
    productsite = Productsite.query.filter_by(productsite_id=request.json['productsite_id']).first()
    productsite.productsite_name = request.json['productsite_name']
    db.session.commit()
    return jsonify({'message': 'Productsite updated!'})


# @app.route('/productsites/delete', methods=['POST'])
# def delete_productsite():
#     Productsite.query.filter_by(productsite_id=request.json['productsite_id']).delete()
#     db.session.commit()
#     return jsonify({'message': 'Productsite deleted!'})


@productitems_blueprint.route('/pharm/productitems/get/<productsite_id>', methods=['GET'])
def get_productitems(productsite_id):
    return jsonify({'productitems': Productitem.query.filter_by(productsite_id=productsite_id).order_by(desc('productitem_time_created'))[:3]})


@productitems_blueprint.route('/pharm/productitem/get/<productitem_id>', methods=['GET'])
def get_productitem(productitem_id):
    return jsonify({'productitem': Productitem.query.filter_by(productitem_id=productitem_id).first()})


@productitems_blueprint.route('/pharm/productitems/add', methods=['POST'])
def add_productitem():
    db.session.add(
        Productitem(productsite_id=request.json['productsite_id'], productitem_name=request.json['productitem_name'],
                    productitem_batch=request.json['productitem_batch'], productitem_shift=request.json['productitem_shift'],
                    productitem_date=request.json['productitem_date'], dayString=request.json['dayString'],
                    productitem_type_sanitary=request.json['productitem_type_sanitary'],
                    productitem_checked="INICIALICED"))
    db.session.commit()
    return jsonify({'message': 'Productitem added!'})


@productitems_blueprint.route('/pharm/productitems/update', methods=['PUT'])
def update_productitem():
    productitem = Productitem.query.filter_by(productitem_id=request.json['productitem_id']).first()
    productitem.productitem_name = request.json['productitem_name']
    productitem.productitem_batch = request.json['productitem_batch']
    productitem.productitem_shift = request.json['productitem_shift']
    productitem.productitem_date = request.json['productitem_date']
    productitem.dayString = request.json['dayString']
    productitem.productitem_type_sanitary = request.json['productitem_type_sanitary']
    productitem.productitem_checked = request.json['productitem_checked']
    db.session.commit()
    return jsonify({'message': 'Productitem updated!'})


if __name__ == '__main__':
    app.run()