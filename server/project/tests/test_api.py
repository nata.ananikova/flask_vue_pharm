import requests
from decouple import config


APP_CURRENT_IP = config('APP_CURRENT_IP')

def test_all_productsites():
    res = requests.get(f"http://{APP_CURRENT_IP}:5000/pharm/productsites/get/all")
    assert res.status_code == 200
    assert res.json()['productsites'][0]['productsite_id'] == 1
    assert res.json()['productsites'][0]['productsite_name'] == 'PTM'
    assert res.json()['productsites'][0]['factoruser_id'] == 1


def test_add_productsite():
    productsite = {"productsite_name": "Coating", "factoruser_id": 1}
    res = requests.post(f"http://{APP_CURRENT_IP}:5000/pharm/productsites/add", json=productsite)
    assert res.status_code == 200
    assert res.json()['message'] == 'Productsite added!'


def test_update_productsite():
    productsite = {"productsite_name": "Tableting", "productsite_id": 2, "factoruser_id": 1}
    res = requests.put(f"http://{APP_CURRENT_IP}:5000/pharm/productsites/update", json=productsite)
    assert res.status_code == 200
    assert res.json()['message'] == 'Productsite updated!'




def test_get_first_productsite():
    res = requests.get(f"http://{APP_CURRENT_IP}:5000/pharm/productitems/get/1")
    assert res.status_code == 200
    assert res.json()['productitems'] == []


def test_add_first_productitem_first_productsite():
    productitem = {"productsite_id": "1", "productitem_name": "Torasemide", "productitem_batch": "200222",
                   "productitem_shift": "1", "productitem_date": "2022-02-02", "dayString": "Sunday",
                   "productitem_type_sanitary": "General", "productitem_checked":"INICIALICED"}
    res = requests.post(f"http://{APP_CURRENT_IP}:5000/pharm/productitems/add", json=productitem)
    assert res.status_code == 200
    assert res.json()['message'] == 'Productitem added!'


def test_update_first_productitem_checked_first_productsite():
    productitem = {"productitem_id": "1", "productitem_name": "Torasemide", "productitem_batch": "200222",
                   "productitem_shift": "1", "productitem_date": "2022-02-02", "dayString": "Sunday",
                   "productitem_type_sanitary": "General", "productitem_checked": "READY FOR WORK"}
    res = requests.put(f"http://{APP_CURRENT_IP}:5000/pharm/productitems/update", json=productitem)
    assert res.status_code == 200
    assert res.json()['message'] == 'Productitem updated!'
