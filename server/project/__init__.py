import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from flask_migrate import Migrate


# instantiate the extensions
db = SQLAlchemy()
migrate = Migrate()


def create_app(script_info=None):

    DATABASE_URL = os.environ.get('DATABASE_URL')
    # instantiate the app
    app = Flask(__name__)
    app.config["SQLALCHEMY_DATABASE_URI"] = f'{DATABASE_URL}'

    # enable CORS
    CORS(app)

    # set config
    app_settings = os.getenv('APP_SETTINGS')
    # app.config.from_object(app_settings)

    # set up extensions
    db.init_app(app)
    migrate.init_app(app, db)

    # register blueprints
    from project.api.productsites import productsites_blueprint, productitems_blueprint, factorusers_blueprint
    app.register_blueprint(factorusers_blueprint)
    app.register_blueprint(productsites_blueprint)
    app.register_blueprint(productitems_blueprint)

    # shell context for flask cli
    @app.shell_context_processor
    def ctx():
        return {'app': app, 'db': db}

    return app
