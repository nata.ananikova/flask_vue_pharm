### Run Flask-Vue-SPA with K8s locally 

   ```sh
   git clone https://gitlab.com/nata.ananikova/flask_vue_pharm.git
   ```
   ```sh
   cd flask_vue_pharm
   ```
   ```sh
   minikube addons enable ingress
   ```
   ```sh
   kubectl delete -A ValidatingWebhookConfiguration ingress-nginx-admission
   ```
   ```sh
   minikube tunnel
   ```
Other terminal
   ```sh
   cd flask_vue_pharm
   ```
   ```sh
   kubectl apply -f kubernetes.yaml  
   ```
   ```sh
   kubectl get pods
   ```
   ```sh
   kubectl exec postgres-{your_pod_index} --stdin --tty -- createdb -U nata demo_db
   ```
   ```sh
   kubectl exec flaskapp-{your_pod_index} --stdin --tty -- python manage.py recreate_db
   ```
   ```sh
   kubectl exec flaskapp-{your_pod_index} --stdin --tty -- python manage.py seed_db
   ```
Stop app
   ```sh
   kubectl delete -f kubernetes.yaml  
   ```

### Run Flask-Vue-SPA with Helm locally 

   ```sh
   git clone https://gitlab.com/nata.ananikova/flask_vue_pharm.git
   ```
   ```sh
   cd flask_vue_pharm
   ```
   ```sh
   minikube addons enable ingress
   ```
   ```sh
   kubectl delete -A ValidatingWebhookConfiguration ingress-nginx-admission
   ```
   ```sh
   minikube tunnel
   ```
Other terminal
   ```sh
   cd flask_vue_pharm
   ```
   ```sh
   helm install -f helm_dir/helm_values/postgres-values.yaml postgres helm_dir/helmpostgres  
   ```
   ```sh
   helm install -f helm_dir/helm_values/flaskapp-values.yaml flaskapp helm_dir/helmpharm    
   ```
   ```sh
   helm install -f helm_dir/helm_values/vue-values.yaml vue helm_dir/helmvue  
   ```
   ```sh
   kubectl get pods
   ```
   ```sh
   kubectl exec postgres-{your_pod_index} --stdin --tty -- createdb -U nata demo_db
   ```
   ```sh
   kubectl exec flaskapp-{your_pod_index} --stdin --tty -- python manage.py recreate_db
   ```
   ```sh
   kubectl exec flaskapp-{your_pod_index} --stdin --tty -- python manage.py seed_db
   ```
Stop app
   ```sh
   helm uninstall postgres  
   ```
   ```sh
   helm uninstall flaskapp  
   ```
   ```sh
   helm uninstall vue  
   ```
### Run Flask-Vue-SPA with docker-compose locally

   ```sh
   git clone https://gitlab.com/nata.ananikova/flask_vue_pharm.git
   ```
   ```sh
   cd flask_vue_pharm
   ```
   ```sh
   git clone -b terraform_ansible_ec2 https://gitlab.com/nata.ananikova/flask_vue_client.git
   ```
   ```sh
   cd flask_vue_client
   ```
   ```sh
   touch .env
   ```
   ```sh
   echo 'VUE_APP_CURRENT_IP="127.0.0.1"' > .env
   ```
   ```sh
   npm install
   ```
   ```sh
   npm run build
   ```
   ```sh
   cd ..
   ```
   ```sh
   touch .env
   ```
   ```sh
   echo 'FLASK_APP=project/__init__.py' > .env
   ```
   ```sh
   echo 'FLASK_DEBUG=1' >> .env
   ```
Insert your values
   ```sh
   echo 'DATABASE_URL=postgresql://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{SQL_HOST}:{SQL_PORT}/{POSTGRES_DB}' >> .env
   ```
   ```sh
   echo 'SQL_HOST=' >> .env
   ```
   ```sh
   echo 'SQL_PORT=' >> .env
   ```
   ```sh
   echo 'DATABASE=postgres' >> .env
   ```
   ```sh
   echo 'APP_CURRENT_IP=' >> .env
   ```
   ```sh
   touch .env_db
   ```
Insert your values
   ```sh
   echo 'POSTGRES_USER=' > .env_db
   ```
   ```sh
   echo 'POSTGRES_PASSWORD=' >> .env_db
   ```
   ```sh
   echo 'POSTGRES_DB=' >> .env_db
   ```
   ```sh
   echo 'SQL_HOST=' >> .env_db
   ```
   ```sh
   echo 'SQL_PORT=' >> .env_db
   ```
   ```sh
   docker-compose up -d --build
   ```
   ```sh
   docker ps
   ```
enter to postgres container
   ```sh
   docker-compose exec db psql --username={POSTGRES_USER} --dbname={POSTGRES_DB}
   ```
   ```sh
   docker-compose exec web python manage.py recreate_db 
   ```
   ```sh
   docker-compose exec web python manage.py seed_db 
   ```
testing API
   ```sh
   docker-compose exec web python -m pytest 
   ```
### Prometheus - Monitoring system & time series database
   ```sh
   helm repo add prometheus-community https://prometheus-community.github.io/helm-charts 
   ```
   ```sh
   helm repo list 
   ```
   ```sh
   helm repo update 
   ```
   ```sh
   kubectl create namespace monitoring 
   ```
   ```sh
   helm install monitoring prometheus-community/kube-prometheus-stack -n monitoring  
   ```
   ```sh
   kubectl --namespace monitoring get pods -l "release=monitoring" 
   ```
   ```sh
   kubectl get all -n monitoring 
   ```
   ```sh
   kubectl port-forward service/monitoring-kube-prometheus-prometheus -n monitoring 9090:9090
   ```
### Grafana: The open observability platform
   ```sh
   kubectl port-forward service/monitoring-grafana -n monitoring 8080:80
   ```
Default credentials: user: admin, pwd: prom-operator 
search: manage
search: Kubernetes/Compute Resources/Cluster

### Connect to EKS via GUI manually

export AWS_ACCESS_KEY_ID=
export AWS_SECRET_ACCESS_KEY=
export AWS_DEFAULT_REGION=eu-central-1
aws configure list 
aws eks update-kubeconfig --name eks-cluster-test
cat .kube/config
kubectl get nodes

### Connect to EKS via eksctl (CLI)

#### Install eksctl to macOS
brew tap weaveworks/tap
brew install weaveworks/tap/eksctl

#### Connect to EKS via eksctl (CLI)
export AWS_ACCESS_KEY_ID=
export AWS_SECRET_ACCESS_KEY=
export AWS_DEFAULT_REGION=eu-central-1
